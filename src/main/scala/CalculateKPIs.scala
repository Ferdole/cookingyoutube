package org.cookinga
import Analytics.ComputeKPIs

import org.cookinga.Utils.{SparkFileHandler, SparkSessionItem}

object CalculateKPIs extends SparkSessionItem {

  def main(args: Array[String]): Unit = {

    val parquetFilePath = args(0)
    val jsonOutputPath = args(1)

    val rawYoutubeInfo = SparkFileHandler.readParquet(parquetFilePath)
    rawYoutubeInfo.cache()

    val mostIngredientsRecipe = ComputeKPIs.mostIngredientsForRecipe(rawYoutubeInfo)
    println(mostIngredientsRecipe)

    val longestVid = ComputeKPIs.longestDurationCookingRecipe(rawYoutubeInfo)
    println(longestVid)

    longestVid.writeToJSON(jsonOutputPath)
    mostIngredientsRecipe.writeToJSON(jsonOutputPath)

    rawYoutubeInfo.unpersist()
  }


}
