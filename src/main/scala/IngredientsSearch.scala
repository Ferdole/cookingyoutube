package org.cookinga

import Analytics.MatchIngredients

import org.cookinga.Utils.SparkFileHandler

object IngredientsSearch {

  /**
   * Read list of ingredients
   * Read parquet
   * Search provided ingredients in data set
   * */
  def main(args: Array[String]): Unit = {

    if (args.length == 0) {
      println("No arguments have been passed")
      sys.exit(1)
    }

    val parquetPath = args(0)
    val ingredients: Seq[String] = args(1).split(",").map(s => s.toLowerCase())

    val youtubeDF = SparkFileHandler.readParquet(parquetPath)

    val topMatchingRecipes = MatchIngredients.matchIngredients(youtubeDF, ingredients)

    println(s"Top matching recipes ${topMatchingRecipes.mkString(" ")} ")
  }
}