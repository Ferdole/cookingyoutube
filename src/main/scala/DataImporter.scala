package org.cookinga

import org.apache.spark.sql
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession, functions}
import org.apache.spark.sql.functions.{collect_list, struct, udf}
import org.apache.spark.sql.types.{IntegerType, LongType, StringType, StructType}
import org.apache.spark.sql.functions._
import org.cookinga.Analytics.{ComputeKPIs, MatchIngredients}
import org.cookinga.Utils.SparkFileHandler

object DataImporter {

  def main(args: Array[String]): Unit = {

    if (args.length == 0) {
      println("No arguments have been passed")
      sys.exit(1)
    }

    val youtubeFilePath = args(0)
    val parquetOutput = args(1)

    val schema = new StructType()
      .add("videoId", StringType)
      .add("startTime", LongType)
      .add("endTime", LongType)
      .add("action", StringType)
      .add("ingredients", StringType)

    // TODO refactor to a config file
    val CSVReadOptions = Some(Map("header" -> "false", "delimiter" -> ","))

    val rawYoutubeInfo = SparkFileHandler.readCSVFileWithSchema(youtubeFilePath, schema, CSVReadOptions)
    rawYoutubeInfo.cache()

    SparkFileHandler.writeParquet(parquetOutput, rawYoutubeInfo)
    println(s"Wrote parquet to ${parquetOutput}")

    println("Finished importing data")
}

  def detailDataframe(df: DataFrame): Unit= {
    df.explain()
  }
}
