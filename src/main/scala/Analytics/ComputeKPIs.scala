package org.cookinga
package Analytics

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, functions}

case class LongestRecipe(youtubeID: String, endTime: Long) {
  override def toString: String = s"videoId: ${youtubeID} with duration ${endTime}"

  def writeToJSON(outputPath: String): Unit = {
    os.write.over(os.Path(outputPath)/"LongestRecipe.json", ujson.write(ujson.Obj("videoID" -> youtubeID, "endTime" -> endTime), indent=4))
  }
}

case class MostIngredientsRecipe(youtubeID: String, ingredientCount: Int){
  override def toString: String = s"videoID: ${youtubeID} with total ingredient count ${ingredientCount}"

  def writeToJSON(outputPath: String): Unit = {
    os.write.over(os.Path(outputPath)/"mostIngredientsRecipe.json", ujson.write(ujson.Obj("videoID" -> youtubeID, "ingredientCount" -> ingredientCount), indent=4))
  }
}

object ComputeKPIs {

  /** Get the youtube video that is the longest */
  def longestDurationCookingRecipe(videoDF: DataFrame): LongestRecipe ={

    val longestRecipeRow =  videoDF
      .groupBy("videoId")
      .agg(max("endTime").alias("maxEndTime"))
      .sort(desc("maxEndTime")).take(1).head

    LongestRecipe(longestRecipeRow.getAs[String]("videoId"),
      longestRecipeRow.getAs[Long]("maxEndTime"))
  }

  /** Get the youtube recipe that uses the most ingredients */
  def mostIngredientsForRecipe(videoDF: DataFrame): MostIngredientsRecipe = {
    // TODO add filter for ingredients count over 20 for a step, remove video ID from table in that case bc data is skewed?

    val ingredientsDF = videoDF
      .withColumn("splittedIngredients", functions.split(col("ingredients"), " "))
      .groupBy("videoId")
      .agg(functions.size(array_distinct(flatten(collect_set("splittedIngredients")))).as("ingredientCount"))

    val top_ingredients = ingredientsDF.sort(desc("ingredientCount")).take(1).head
    MostIngredientsRecipe(top_ingredients.getAs[String]("videoId"), top_ingredients.getAs[Int]("ingredientCount"))
  }

  /** Most complex recipe to cook based on actions, time spent on actions from total time */

}
