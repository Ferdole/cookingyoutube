package org.cookinga
package Analytics

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{asc, col, collect_set, count, desc, expr, sum, udf}

case class ingredientsBestMatchRecipe(matchPosition: Int, videoId: String)

object MatchIngredients
{

  /**
   * Search each provided ingredient in the "Ingredients" column. The problem with this approach is that if the list
   * contains Seq("sugar", "brown sugar") and "Ingredients='brown sugar'" it will count the match two time.
   * It is not totally correct since when summing the found ingredients multiple duplicates found will get added
   * */
  def matchIngredients(ytDF: DataFrame, searchedIngredients: Seq[String]) = {
    val topMatchingRecipe = ytDF
      .filter(col("ingredients").isNotNull)
      .withColumn("foundIngredients", countFoundIngredients(searchedIngredients)(col("Ingredients")))
      .groupBy("videoId")
      .agg(
        sum("foundIngredients").as("foundIngredients"),
        count("action").as("actions"),
        collect_set("ingredients").as("allIngredients")
      )
      .sort(desc("foundIngredients"), asc("actions"))  // if foundIngredients are equal a better
    // match would be the less actions are taken for the recipe, since one ingredient can match multiple actions
      .filter(col("foundIngredients") > 0 )
      .take(5)
      .map ( r => r.getAs[String]("videoId"))
    topMatchingRecipe
  }

  private def countFoundIngredients(searchedIngredients: Seq[String]): UserDefinedFunction = {

    udf[Int, String] ( (colIngredients: String) => {
      searchedIngredients.map(ingredient => {
        if (colIngredients.contains(ingredient)) 1
        else 0
      }).sum
    })
  }

}
