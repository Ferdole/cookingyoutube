package org.cookinga
package Utils

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

// TODO refactor to something else, so it can take a Configuration argument
trait SparkSessionItem {
  private val master = "local[1]"
  private val appName = "CookingYoutube"
  private val conf: SparkConf = new SparkConf()
    .setAppName(appName)
    .set("spark.sql.shuffle.partitions", "8") // runs with 2 cores
//    .setMaster(master)
//    .set("spark.driver.host", "127.0.0.1")
//    .set("log4j.rootCategory", "WARN, console")
//    .set("spark.local.dir", "/mnt/app/spark_junk")
  val ss: SparkSession = SparkSession.builder().config(conf).getOrCreate()
  ss.sparkContext.setLogLevel("WARN")

}
