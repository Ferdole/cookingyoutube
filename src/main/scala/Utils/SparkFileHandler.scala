package org.cookinga
package Utils

import org.apache.spark.sql.{DataFrame, SaveMode}
import org.apache.spark.sql.types.StructType

object SparkFileHandler extends SparkSessionItem {

  def readCSVFileWithSchema(filePath: String, schema: StructType, options: Option[Map[String, String]]): DataFrame = {
    ss.read.format("csv")
      .options(options match {
        case Some(options) => options
        case None => Map[String, String]()
      })
      .schema(schema)
      .load(filePath)
  }

  def readParquet(filePath: String): DataFrame = {
    ss.read.parquet(filePath)
  }

  def writeParquet(fileOutput: String, df: DataFrame): Unit = {
    df.write
      .mode(SaveMode.Overwrite)
      .parquet(fileOutput)
  }


}
