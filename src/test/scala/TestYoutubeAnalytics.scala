package org.cookinga

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{LongType, StringType, StructType}
import org.cookinga.Analytics.{ComputeKPIs, LongestRecipe, MatchIngredients, MostIngredientsRecipe}
import org.scalatest.{Matchers, WordSpec}


class DummyTest extends WordSpec with SparkSessionT with Matchers {

  def fixture = new {
    val searchedIngredients = Seq("flour", "sugar")
  }

  "Input test_data1.csv" should {
    s"return top recipes Array('3', '1') for ingredients list: ${fixture.searchedIngredients.mkString(", ")}" in withSparkSession {
      (sparkSession: SparkSession) => {
        val schema = new StructType()
          .add("videoId", StringType)
          .add("startTime", LongType)
          .add("endTime", LongType)
          .add("action", StringType)
          .add("ingredients", StringType)

        val youtubeDF = sparkSession.read.format("csv")
          .schema(schema)
          .load("src/test/test_data/test_data1.csv")

        val returnedRecipes = MatchIngredients.matchIngredients(youtubeDF, fixture.searchedIngredients)
        println(returnedRecipes.mkString(" "))
        assert(Array("3", "1") sameElements returnedRecipes)
      }
    }
  }

  it should {
    "return MostIngredientsRecipe(\"3\", 5) by number of ingredients" in withSparkSession {
      (sparkSession: SparkSession) => {
        val schema = new StructType()
          .add("videoId", StringType)
          .add("startTime", LongType)
          .add("endTime", LongType)
          .add("action", StringType)
          .add("ingredients", StringType)

        val youtubeDF = sparkSession.read.format("csv")
          .schema(schema)
          .load("src/test/test_data/test_data1.csv")

        val mostIngredientsRecipes: MostIngredientsRecipe = ComputeKPIs.mostIngredientsForRecipe(youtubeDF)
        println(mostIngredientsRecipes)
        assert(MostIngredientsRecipe("3", 5) == mostIngredientsRecipes)
      }
    }
  }
  // test longest recipe by duration
  it should {
    "return LongestRecipe(\"2\", 1658799) by max(endTime)" in withSparkSession {
      (sparkSession: SparkSession) => {
        val schema = new StructType()
          .add("videoId", StringType)
          .add("startTime", LongType)
          .add("endTime", LongType)
          .add("action", StringType)
          .add("ingredients", StringType)

        val youtubeDF = sparkSession.read.format("csv")
          .schema(schema)
          .load("src/test/test_data/test_data1.csv")

        val longestRecipe: LongestRecipe = ComputeKPIs.longestDurationCookingRecipe(youtubeDF)
        println(longestRecipe)
        assert(LongestRecipe("2", 1658799) == longestRecipe)
      }
    }
  }
}

trait SparkSessionT {

    def withSparkSession(testMethod: (SparkSession) => Any): Unit = {
      val master = "local[1]"
      val appName = "CookingYoutube"
      val conf: SparkConf = new SparkConf()
        .setAppName(appName)
        .set("spark.sql.shuffle.partitions", "8")
        .setMaster(master)
        .set("spark.driver.host", "127.0.0.1")
        .set("log4j.rootCategory", "WARN, console")
        .set("spark.local.dir", "/mnt/app/spark_junk")
      val ss: SparkSession = SparkSession.builder().config(conf).getOrCreate()
      ss.sparkContext.setLogLevel("WARN")

      try {
        testMethod(ss)
      }
      finally ss.stop()
    }
  }

