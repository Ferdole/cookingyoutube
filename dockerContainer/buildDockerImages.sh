#!/bin/bash

docker build -t spark-base dockerContainer/docker/base/
docker build -t spark-master dockerContainer/docker/spark-master/
docker build -t spark-worker dockerContainer/docker/spark-worker/
docker build -t sbt dockerContainer/docker/sbt-build/