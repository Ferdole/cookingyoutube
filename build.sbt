name := "CookingAssignment"

version := "0.2"

scalaVersion := "2.12.10"
val sparkVersion = "3.1.1"

libraryDependencies ++=Seq(
  //TODO add to "Test" in order to run tests
  "org.apache.spark" %% "spark-core" % sparkVersion % "provided",
  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided",
  //TODO change to Playframework JSON library
  "com.lihaoyi" %% "upickle" % "0.9.5",
  "com.lihaoyi" %% "os-lib" % "0.7.1"
)

// Test Specific
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % Test
parallelExecution in Test := false

// Exclude Scala library JARs, they are provided by the spark cluster
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

unmanagedResourceDirectories in Compile += { baseDirectory.value / "src/main/resources/*" }

idePackagePrefix := Some("org.cookinga")
