# CookingAssignment

## 1. Setup and run

`chmod +x run.sh` - Give execution permission for script

`sudo ./run.sh` - Run the script to generate the fat jar file and run deploy the spark-cluster

You should open a new terminal since the containers connect to stdout #TODO check how to prevent this

`sudo ./run_tests.sh` - should run tests in a docker container and output exit code if they fail ### TODO

Containers used:
  * spark standalone cluster single worker node
  * container to build the fat jar with `sbt assembly`
  * container to run tests ###TODO

The script builds the fat jar with sbt, downloads the latest csv data and deploys a single node spark standalone cluster using inspiration from https://github.com/mvillarrealb/docker-spark-cluster/blob/master/docker-compose.yml.

If run on local machine you can check spark UI on http://localhost:8080 and from the outside with http://{hosts_ip}:9090

## 2. Run applications

Results are visible in the `./data` folder or on the container `/opt/spark-data/`

### Run the import process:

`sudo docker exec spark-worker /spark/bin/spark-submit --class org.cookinga.DataImporter --driver-memory 1g --executor-memory 512m --total-executor-cores 2 --master spark://spark-master:7077 /opt/spark-apps/CookingAssignment-assembly-0.2.jar /opt/spark-data/exportable_action_objects_youtube.csv /opt/spark-data/rawYoutube/yt.parquet`

The App takes 2 arguments relative to docker container:
  1. absolute path of source CSV
  2. absolute path of parquet destination


### Run the analytics application and calculate some key KPI values:

`sudo docker exec spark-worker /spark/bin/spark-submit --class org.cookinga.CalculateKPIs --driver-memory 1g --executor-memory 512m --total-executor-cores 2 --master spark://spark-master:7077 /opt/spark-apps/CookingAssignment-assembly-0.2.jar /opt/spark-data/rawYoutube/yt.parquet /opt/spark-data/`

The App takes 2 arguments relative docker container:
  1. absolute path of parquet file
  2. absolute path of folder where to output KPI JSON files: 
     - `LongestRecipe.json`
     - `mostIngredientsRecipe.json`

Check KPIs with `cat ./data/{filename.json}`


### Run the batch job to find the top matching recipes for provided ingredients list, use:

`sudo docker exec spark-worker /spark/bin/spark-submit --class org.cookinga.IngredientsSearch --driver-memory 1g --executor-memory 512m --total-executor-cores 2 --master spark://spark-master:7077 /opt/spark-apps/CookingAssignment-assembly-0.2.jar /opt/spark-data/rawYoutube/yt.parquet "brown sugar,salami,wheat flour"`

The App takes 2 arguments:
  1. absolute path of parquet file
  2. ingredients in a quoted list with no spaces between commas eg. `"brown sugar,tomatos,wheat flour".
     - if recipes have same number of ingredients the recipe with lowest action count is first
