#!/bin/bash
#set -x

# build the docker container images needed
source ./dockerContainer/buildDockerImages.sh

# build the fat jar file
docker run --rm -v $(pwd):/cooking -v $(pwd)/apps:/build sbt /bin/bash -c "cd /cooking/; sbt clean; sbt assembly; cp target/scala-2.12/CookingAssignment-assembly-0.2.jar /build"

# download input data
mkdir ./data
wget https://storage.googleapis.com/whats_cookin/whats_cookin.zip -O data/cooking_data.zip
unzip -o data/cooking_data.zip -d data

docker-compose up